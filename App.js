import React from 'react';

import {StyleSheet, SafeAreaView, FlatList, TouchableOpacity, View, Image, TextInput as RNTextInput, Text as RNText } from 'react-native';
import { compose, typography, border, shadow, space, color } from 'styled-system'
import { NavigationContainer, DefaultTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { StatusBar } from 'expo-status-bar';
import styled from 'styled-components/native'
import useSWR from 'swr'
import ky from 'ky'

import logo from './assets/logo.png'

const Stack = createStackNavigator();

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#4995E7',
    background: '#FFFFFF'
  },
};

export default function App() {
  return (
    <NavigationContainer theme={MyTheme}>
      <Stack.Navigator>
        <Stack.Screen name="Landing" component={LandingScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Movie" component={MovieScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const LandingScreen = ({ navigation }) => {
  return (
    <Box flex={1} py={'40%'} alignItems="center" justifyContent="space-between">
        <Image
          source={logo}
          resizeMethod="scale"
          style={{ width: 200, height: 100 }}
        />

        <Text fontWeight="bold" fontSize={4}>WELCOME</Text>

        <Button onPress={() => navigation.navigate('Login')}>
          <Text color="white" fontWeight="bold">GETTING START</Text>
        </Button>
    </Box>
  );
}

const LoginScreen = ({ navigation }) => {
  return (
    <Box flex={1} py="40%" alignItems="center" justifyContent="space-between">
      <Image
        source={logo}
        resizeMethod="scale"
        style={{ width: 200, height: 100 }}
      />

      <Box>
        <Input
          width={300}
          borderWidth={1}
          borderColor="#E1E1E1"
          borderRadius={30}
          p={3}
          placeholder="User Name"
        />

        <Input
          mt={3}
          width={300}
          borderWidth={1}
          borderColor="#E1E1E1"
          borderRadius={30}
          p={3}
          placeholder="Password"
        />
      </Box>

      <Button onPress={() => navigation.navigate('Movie')}>
        <Text color="white" fontWeight="bold">LOGIN</Text>
      </Button>
    </Box>
  );
}


const MovieScreen = () => {
  const fetcher = (...args) => ky(...args).then(res => res.json())

  const renderItem = ({ item }) => (
    <Box flexDirection="row" border='1px solid #E1E1E1' borderRadius={4} bg="white" mx={3} my={2}>
      <Box width={120}>
        <Image
          source={{ uri: item.medium_cover_image }}
          resizeMethod="resize"
          style={{ width: 120, height: 150 }}
        />
      </Box>
      <Box p={3} width="70%">
        <Text fontWeight="bold" fontSize={2}>{item.title_long}</Text>
        <Text mt={3}>⭐️⭐️⭐️⭐️⭐️</Text>
        <Text mt={3} color="#4995E7">{item.genres.join(' | ')}</Text>
      </Box>
    </Box>
  )

  const { data, error } = useSWR('https://yts.mx/api/v2/list_movies.json?quality=3D', fetcher)

  if (!data) {
    return (
      <SafeAreaView style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Loading...</Text>
      </SafeAreaView>
    )
  }

  console.log(data)

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <FlatList
        data={data.data.movies}
        renderItem={renderItem}
        keyExtractor={item => item.imdb_code}
      />
    </SafeAreaView>
  );
}

/**
 * Custom Components
 */

const Box = styled(View)(
  compose(
    border,
    shadow,
    space,
    color
  )
)

const Input = styled(RNTextInput)(
  compose(
    border,
    shadow,
    space,
    color
  )
)

const Text = styled(RNText)(
  compose(
    typography,
    space,
    color
  )
)

const Button = styled.TouchableOpacity`
  background-color: #4995E7;
  border-radius: 30px;
  padding: 20px;
`

